<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{asset('css/login.css')}}">
    <title>Login</title>
</head>
<body>
    <div class="login-page">
        <div class="form">
            <form class="login-form" action="{{route('login')}}" method="POST">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                @if(Session::has('message'))
                    {{Session::get('message')}}
                @endif
                <br/>
                <label>User Name:</label>
                <input type="text" name="user" required /><br/>
                <label>Password:</label>
                <input type="password" name="pass" required/><br/>
                <button type="submit">Login</button>
            </form>
        </div>
    </div>
</body>
</html>