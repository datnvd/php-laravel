<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class _User extends Model
{
    protected $table = "_user";
}
