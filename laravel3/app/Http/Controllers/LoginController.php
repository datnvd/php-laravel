<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\_User;
class LoginController extends Controller
{
    public function getLogin(){
        return view('login');
    }
    public function postLogin(Request $request){
        $check=['user_name'=>$request->user,
                'password'=>$request->pass];
        if(DB::table('_user')->where($check)->count()==1){
            return redirect()->route('success');
        }else{
            return redirect()->back()->with('message','Dang nhap that bai. Kiem tra lai username va password');
        }
    }
}