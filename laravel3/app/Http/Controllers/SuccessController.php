<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class SuccessController extends Controller
{
	public function getSuccess()
	{
		return view('success');
	}
}